package capitalria.mk.moviesfragment.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

public class DialogManagerImpl implements DialogManger {
    private InfoDialog mInfoDialog;
    private TwoOptionsDialog mTwoOptionsDialog;

    @Override

    public void showInfoDialog(Context context, String title, String message, String buttonLabel, boolean isCancelable, View.OnClickListener onClickListener, DialogInterface.OnDismissListener onDismissListener) {
        mInfoDialog = InfoDialog.newInstance(title, message, buttonLabel, isCancelable);
        mInfoDialog.setOnClickListener(onClickListener);
        mInfoDialog.setOnDismissListener(onDismissListener);
        mInfoDialog.show(context);
    }

    public void showInfoDialog(Context context, int title, int message, int buttonLabel, boolean isCancelable, View.OnClickListener onClickListener, DialogInterface.OnDismissListener onDismissListener) {
        String titleStr = getStringLabels(context, title);
        String messageStr = getStringLabels(context, message);
        String buttonLabelStr = getStringLabels(context, buttonLabel);
        showInfoDialog(context, titleStr, messageStr, buttonLabelStr, isCancelable, onClickListener, onDismissListener);
    }


    @Override
    public void hideInfoDialog() {
        if (mInfoDialog != null) {
            mInfoDialog.dismiss();
            mInfoDialog = null;
        }
    }

    @Override
    public void showTwoOptionsDialog(Context context, int message, int leftButtonLabel, int rightButtonLabel, boolean cancelable, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener, DialogInterface.OnDismissListener onDismissListener) {
        showTwoOptionsDialog(context, -1, message, leftButtonLabel, rightButtonLabel, cancelable, leftClickListener, rightClickListener, onDismissListener);
    }

    @Override
    public void showTwoOptionsDialog(Context context, int title, int message, int leftButtonLabel, int rightButtonLabel, boolean cancelable, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener, DialogInterface.OnDismissListener onDismissListener) {
        String titleStr = getStringLabels(context, title);
        String messageStr = getStringLabels(context, message);
        String leftButtonStr = getStringLabels(context, leftButtonLabel);
        String rightButtonStr = getStringLabels(context, rightButtonLabel);
        showTwoOptionsDialog(context, titleStr, messageStr, leftButtonStr, rightButtonStr, cancelable, leftClickListener, rightClickListener, onDismissListener);
    }

    @Override
    public void showTwoOptionsDialog(Context context, String message, String leftButtonLabel, String rightButtonLabel, boolean cancelable, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener, DialogInterface.OnDismissListener onDismissListener) {
        showTwoOptionsDialog(context, null, message, leftButtonLabel, rightButtonLabel, cancelable, leftClickListener, rightClickListener, onDismissListener);
    }


    @Override
    public void showTwoOptionsDialog(Context context, String title, String message, String leftButtonLabel, String rightButtonLabel, boolean cancelable, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener, DialogInterface.OnDismissListener onDismissListener) {
        mTwoOptionsDialog = TwoOptionsDialog.newInstance(title, message, leftButtonLabel, rightButtonLabel, cancelable);
        mTwoOptionsDialog.setOnDismissListener(onDismissListener);
        mTwoOptionsDialog.setOnLeftClickListener(leftClickListener);
        mTwoOptionsDialog.setOnRightClickListener(rightClickListener);
        mTwoOptionsDialog.show(context);
    }

    @Override
    public void hideTwoOptionsDialog() {
        if (mTwoOptionsDialog != null) {
            mTwoOptionsDialog.dismiss();
            mTwoOptionsDialog = null;
        }
    }

    private String getStringLabels(Context context, int resource) {
        if (context == null) {
            return null;
        }
        return resource == -1 ? null : context.getString(resource);
    }
}
