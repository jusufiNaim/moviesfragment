package capitalria.mk.moviesfragment.dialogs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import capitalria.mk.moviesfragment.R;


/**
 * Dialog containing two Buttons (Options)
 *
 * @author Naim.Jusufi
 */
public class TwoOptionsDialog extends BaseDialog {
    private static final String ARG_TITLE = TwoOptionsDialog.class.getSimpleName() + "_arg_title";
    private static final String ARG_MESSAGE = TwoOptionsDialog.class.getSimpleName() + "_arg_message";
    private static final String ARG_LEFT_BUTTON = TwoOptionsDialog.class.getSimpleName() + "_arg_left_button";
    private static final String ARG_RIGHT_BUTTON = TwoOptionsDialog.class.getSimpleName() + "_arg_right_button";
    private static final String ARG_CANCELABLE = TwoOptionsDialog.class.getSimpleName() + "_arg_cancelable";
    private static final int DEFAULT_VALUE = -1;

    private Button mLeftButton;
    private Button mRightButton;
    private TextView mTitleTextView;
    private TextView mMessageTextView;
    private String mTitle;
    private String mMessage;
    private String mLeftButtonLabel;
    private String mRightButtonLabel;
    private boolean mIsCancelable;
    private View.OnClickListener mOnLeftClickListener;
    private View.OnClickListener mOnRightClickListener;
    private DialogInterface.OnDismissListener mOnDismissListener;

    public static TwoOptionsDialog newInstance(String title, String message, String leftButtonLabel, String rightButtonLabel, boolean isCancelable) {

        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, message);
        args.putString(ARG_LEFT_BUTTON, leftButtonLabel);
        args.putString(ARG_RIGHT_BUTTON, rightButtonLabel);
        args.putBoolean(ARG_CANCELABLE, isCancelable);
        TwoOptionsDialog fragment = new TwoOptionsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static TwoOptionsDialog newInstance(int title, int message, int leftButtonLabel, int rightButtonLabel, boolean isCancelable) {

        Bundle args = new Bundle();
        args.putInt(ARG_TITLE, title);
        args.putInt(ARG_MESSAGE, message);
        args.putInt(ARG_LEFT_BUTTON, leftButtonLabel);
        args.putInt(ARG_RIGHT_BUTTON, rightButtonLabel);
        args.putBoolean(ARG_CANCELABLE, isCancelable);
        TwoOptionsDialog fragment = new TwoOptionsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnLeftClickListener(View.OnClickListener onLeftClickListener) {
        mOnLeftClickListener = onLeftClickListener;
    }

    public void setOnRightClickListener(View.OnClickListener onRightClickListener) {
        mOnRightClickListener = onRightClickListener;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListemer) {
        mOnDismissListener = onDismissListemer;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_two_options;
    }

    @Override
    public boolean isDialogCancelable() {
        return mIsCancelable;
    }

    @Override
    public DialogInterface.OnDismissListener getDialogDismissListener() {
        return mOnDismissListener;
    }

    @Override
    public void getArgs(Bundle arguments) {
        if (arguments == null) {
            return;
        }
        int titleRes = arguments.getInt(ARG_TITLE, DEFAULT_VALUE);
        int messageRes = arguments.getInt(ARG_MESSAGE, DEFAULT_VALUE);
        int leftButtonRes = arguments.getInt(ARG_LEFT_BUTTON, DEFAULT_VALUE);
        int rightButtonRes = arguments.getInt(ARG_RIGHT_BUTTON, DEFAULT_VALUE);
        mTitle = titleRes == DEFAULT_VALUE ? arguments.getString(ARG_TITLE) : getString(titleRes);
        mMessage = messageRes == DEFAULT_VALUE ? arguments.getString(ARG_MESSAGE) : getString(messageRes);
        mLeftButtonLabel = leftButtonRes == DEFAULT_VALUE ? arguments.getString(ARG_LEFT_BUTTON) : getString(leftButtonRes);
        mRightButtonLabel = rightButtonRes == DEFAULT_VALUE ? arguments.getString(ARG_RIGHT_BUTTON) : getString(rightButtonRes);
        mIsCancelable = arguments.getBoolean(ARG_CANCELABLE);
    }

    @Override
    public void initViews(View rootView) {
        mTitleTextView = rootView.findViewById(R.id.title_text_view);
        mMessageTextView = rootView.findViewById(R.id.message_title_view);
        mLeftButton = rootView.findViewById(R.id.left_button);
        mRightButton = rootView.findViewById(R.id.right_button);
    }

    @Override
    public void loadViews() {
        mTitleTextView.setText(mTitle);
        mMessageTextView.setText(mMessage);
        mLeftButton.setText(mLeftButtonLabel);
        mRightButton.setText(mRightButtonLabel);
    }

    @Override
    public void setListeners() {
        mLeftButton.setOnClickListener(mOnLeftClickListener);
        mRightButton.setOnClickListener(mOnRightClickListener);
    }
}
