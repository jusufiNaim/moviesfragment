package capitalria.mk.moviesfragment.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

public interface DialogManger {

    void showInfoDialog(Context context, String title, String message, String buttonLabel, boolean isCancelable, View.OnClickListener onClickListener, DialogInterface.OnDismissListener onDismissListener);

    void showInfoDialog(Context context, int title, int message, int buttonLabel, boolean isCancelable, View.OnClickListener onClickListener, DialogInterface.OnDismissListener onDismissListener);

    void hideInfoDialog();

    void showTwoOptionsDialog(Context context, int message, int leftButtonLabel, int rightButtonLabel, boolean cancelable, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener, DialogInterface.OnDismissListener onDismissListener);

    void showTwoOptionsDialog(Context context, int title, int message, int leftButtonLabel, int rightButtonLabel, boolean cancelable, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener, DialogInterface.OnDismissListener onDismissListener);

    void showTwoOptionsDialog(Context context, String message, String leftButtonLabel, String rightButtonLabel, boolean cancelable, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener, DialogInterface.OnDismissListener onDismissListener);

    void showTwoOptionsDialog(Context context, String title, String message, String leftButtonLabel, String rightButtonLabel, boolean cancelable, View.OnClickListener leftClickListener, View.OnClickListener rightClickListener, DialogInterface.OnDismissListener onDismissListener);

    void hideTwoOptionsDialog();

}
