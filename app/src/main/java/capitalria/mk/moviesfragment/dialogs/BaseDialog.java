package capitalria.mk.moviesfragment.dialogs;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseDialog extends DialogFragment {

    @LayoutRes
    public abstract int getLayoutRes();

    public abstract boolean isDialogCancelable();

    public abstract DialogInterface.OnDismissListener getDialogDismissListener();

    public abstract void getArgs(Bundle arguments);

    public abstract void initViews(View rootView);

    public abstract void loadViews();

    public abstract void setListeners();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            getArgs(getArguments());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutRes(), container, false);
        initViews(rootView);
        if (getDialog() != null) {
            Dialog dialog = getDialog();
            dialog.setCancelable(isDialogCancelable());
            dialog.setOnDismissListener(getDialogDismissListener());
        }
        setCancelable(isDialogCancelable());
        loadViews();
        setListeners();
        return rootView;
    }

    public void show(Context context) {
        if (context instanceof AppCompatActivity) {
            AppCompatActivity activity = (AppCompatActivity) context;
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            show(fragmentManager, this.getClass().getSimpleName());
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
