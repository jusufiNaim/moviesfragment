package capitalria.mk.moviesfragment.dialogs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import capitalria.mk.moviesfragment.R;

/**
 * Dialog for display info messages and one button for confirming the message
 */
public class InfoDialog extends BaseDialog {

    private static final String TITLE_ARG = InfoDialog.class.getSimpleName() + "_title_arg";
    private static final String MESSAGE_ARG = InfoDialog.class.getSimpleName() + "_message_arg";
    private static final String BUTTON_LABEL_ARG = InfoDialog.class.getSimpleName() + "_button_label_arg";
    private static final String CANCELABLE_ARG = InfoDialog.class.getSimpleName() + "_cancelable_arg";
    private static final int DEFAULT_VALUE = -1;
    private String mTitle;
    private String mMessage;
    private String mButtonLabel;
    private boolean mIsCancelable;
    private View.OnClickListener mOnClickListener;
    private DialogInterface.OnDismissListener mOnDismissListener;
    private TextView mTitleTextView;
    private TextView mMessageTextView;
    private Button mConfirmButton;

    public static InfoDialog newInstance(int title, int message, int buttonLabel, boolean cancelable) {

        Bundle args = new Bundle();
        args.putInt(TITLE_ARG, title);
        args.putInt(MESSAGE_ARG, message);
        args.putInt(BUTTON_LABEL_ARG, buttonLabel);
        args.putBoolean(CANCELABLE_ARG, cancelable);

        InfoDialog fragment = new InfoDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static InfoDialog newInstance(String title, String message, String buttonLabel, boolean cancelable) {

        Bundle args = new Bundle();
        args.putString(TITLE_ARG, title);
        args.putString(MESSAGE_ARG, message);
        args.putString(BUTTON_LABEL_ARG, buttonLabel);
        args.putBoolean(CANCELABLE_ARG, cancelable);

        InfoDialog fragment = new InfoDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    @Override
    public void getArgs(Bundle arguments) {
        if (arguments == null) {
            return;
        }
        int titleRes = arguments.getInt(TITLE_ARG, DEFAULT_VALUE);
        int messageRes = arguments.getInt(MESSAGE_ARG, DEFAULT_VALUE);
        int buttonLabelRes = arguments.getInt(BUTTON_LABEL_ARG, DEFAULT_VALUE);
        mTitle = titleRes == DEFAULT_VALUE ? arguments.getString(TITLE_ARG) : getString(titleRes);
        mMessage = messageRes == DEFAULT_VALUE ? arguments.getString(MESSAGE_ARG) : getString(messageRes);
        mButtonLabel = buttonLabelRes == DEFAULT_VALUE ? arguments.getString(BUTTON_LABEL_ARG) : getString(buttonLabelRes);
        mIsCancelable = arguments.getBoolean(CANCELABLE_ARG);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.dialog_info;
    }

    @Override
    public boolean isDialogCancelable() {
        return mIsCancelable;
    }

    @Override
    public DialogInterface.OnDismissListener getDialogDismissListener() {
        return mOnDismissListener;
    }

    @Override
    public void initViews(View rootView) {
        mTitleTextView = rootView.findViewById(R.id.title_text_view);
        mMessageTextView = rootView.findViewById(R.id.message_title_view);
        mConfirmButton = rootView.findViewById(R.id.confirmation_button);
    }

    @Override
    public void loadViews() {
        mTitleTextView.setText(mTitle);
        mMessageTextView.setText(mMessage);
        mConfirmButton.setText(mButtonLabel);
    }

    @Override
    public void setListeners() {
        mConfirmButton.setOnClickListener(mOnClickListener);
    }
}
