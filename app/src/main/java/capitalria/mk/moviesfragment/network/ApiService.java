package capitalria.mk.moviesfragment.network;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import androidx.annotation.Nullable;

import java.util.List;

import capitalria.mk.moviesfragment.helper.MoviesDataSource;
import capitalria.mk.moviesfragment.model.Data;
import capitalria.mk.moviesfragment.model.Example;
import capitalria.mk.moviesfragment.model.Movie;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiService extends IntentService {
    private static final String EXTRA_MOVIE_COUNT = "movie_count";
    private static final String EXTRA_RECEIVER = "receiver";
    public MoviesDataSource moviesDataSource;
    public int totalPages;
    int currentPage = 1;
    YtsApi ytsApi;
    int mMovieCount;
    ResultReceiver mReceiver;
    Bundle recBundle;
    boolean sent = false;

    public ApiService() {
        super("test-service");
    }

    public static Intent newIntent(Context context, int movieCount, ServiceResultReceiver receiver) {
        Intent intent = new Intent(context, ApiService.class);
        intent.putExtra(EXTRA_MOVIE_COUNT, movieCount);
        intent.putExtra(EXTRA_RECEIVER, receiver);
        return intent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // if you override onCreate(), make sure to call super().
        // If a Context object is needed, call getApplicationContext() here.
        moviesDataSource = MoviesDataSource.getsInstance(this);

    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        // This describes what will happen when service is triggered

        mMovieCount = intent.getIntExtra(EXTRA_MOVIE_COUNT, 1);
        mReceiver = intent.getParcelableExtra(EXTRA_RECEIVER);
        calculateApiPages();
        ytsApi = ApiClient.getClient().create(YtsApi.class);
        while (currentPage < totalPages) {
            fetchMovies(currentPage);
            currentPage++;
        }
        recBundle = new Bundle();
    }


    private void calculateApiPages() {
        totalPages = mMovieCount / 50;
        if (mMovieCount % 50 != 0) totalPages++;
    }


    private void fetchMovies(int actualPage) {
        Call<Example> call = ytsApi.getExampleCall(50, actualPage);
        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, retrofit2.Response<Example> response) {
                if (response.isSuccessful()) {
                    responseToDb(response);
                } else {
                    if (!sent) {
                        mReceiver.send(Activity.RESULT_CANCELED, recBundle);
                        sent = true;
                    }
                    return;
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                if (!sent) {
                    mReceiver.send(Activity.RESULT_CANCELED, recBundle);
                    sent = true;
                }
                return;
            }
        });
    }

    private synchronized void responseToDb(Response<Example> response) {
        Data data;
        if ((response.body() != null) && (!response.body().isDataNull())) {

            data = response.body().getData();
            if (data.getMovies() != null) {
                List<Movie> movies = data.getMovies();

                for (Movie movie : movies) {
                    moviesDataSource.createMovieInfo(movie);
                }
                if (!sent) {
                    mReceiver.send(Activity.RESULT_OK, recBundle);
                    sent = true;
                }
            }
            return;
        } else {
            if (!sent) {
                mReceiver.send(Activity.RESULT_CANCELED, recBundle);
                sent = true;
            }
            return;
        }
    }
}

