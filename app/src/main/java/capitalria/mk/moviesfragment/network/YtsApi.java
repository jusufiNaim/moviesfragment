package capitalria.mk.moviesfragment.network;

import capitalria.mk.moviesfragment.model.Example;
import capitalria.mk.moviesfragment.utils.Constants;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by jusuf on 28.8.2017.
 */

public interface YtsApi {

    @GET(Constants.URL_ENDPOINT)
    Call<Example> getExampleCall(
            @Query(Constants.URL_LIMIT_QUERY) int limit,
            @Query(Constants.URL_PAGE_QUERY) int pageIndex
    );
}

