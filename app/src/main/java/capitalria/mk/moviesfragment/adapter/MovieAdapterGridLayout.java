package capitalria.mk.moviesfragment.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import capitalria.mk.moviesfragment.R;
import capitalria.mk.moviesfragment.model.Movie;

/**
 * Created by jusuf on 16.1.2018.
 */

public class MovieAdapterGridLayout extends RecyclerView.Adapter<MovieAdapterGridLayout.ViewHolder> {
    private List<Movie> mDataset;
    private Context context;

    public MovieAdapterGridLayout(Context context, List<Movie> mDataset) {
        this.context = context;
        this.mDataset = mDataset;
    }

    @Override
    public MovieAdapterGridLayout.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_list_item, parent, false);

        return new MovieAdapterGridLayout.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Movie movie = mDataset.get(position);
        holder.tvYear.setText(movie.getYear().toString());
        holder.tvRating.setText(movie.getRating().toString());
        holder.tvGenres.setText(movie.getGenre());
        Picasso.with(context)
                .load(movie.getMediumCoverImage())
                .placeholder(R.drawable.welcome)
                .fit()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView tvYear;
        TextView tvRating;
        TextView tvGenres;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            tvYear = itemView.findViewById(R.id.list_item_year);
            tvRating = itemView.findViewById(R.id.list_item_rating);
            imageView = itemView.findViewById(R.id.grid_movie_image);
            tvGenres = itemView.findViewById(R.id.list_item_genres);
        }
    }
}
