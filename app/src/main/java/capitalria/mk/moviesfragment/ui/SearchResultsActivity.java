package capitalria.mk.moviesfragment.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.NavUtils;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import capitalria.mk.moviesfragment.R;
import capitalria.mk.moviesfragment.adapter.MovieAdapterGridLayout;
import capitalria.mk.moviesfragment.adapter.MovieAdapterLinearLayout;
import capitalria.mk.moviesfragment.adapter.RecyclerTouchListener;
import capitalria.mk.moviesfragment.helper.MoviesDataSource;
import capitalria.mk.moviesfragment.model.Movie;

public class SearchResultsActivity extends AppCompatActivity {
    public static final String KEY_BUNDLE = "bundle";
    private static final String KEY_FIRST_ITEM_ID = "firstItemId";
    private static final String KEY_LAYOUT_MANAGER = "grid";
    protected LinearLayoutManager mLayoutManager;
    HashMap<String, String> sqlParams;
    int firstItemId;
    int lastItemScrollPosition;
    int scrollPosition = 0;
    private MoviesDataSource mMoviesDataSource;
    private List<Movie> mMovieList;
    private RecyclerView mRecyclerView;
    private boolean isGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        if (savedInstanceState != null)
            isGrid = savedInstanceState.getBoolean(KEY_LAYOUT_MANAGER);

        Toolbar myChildToolbar =
                findViewById(R.id.searchResultToolbar);
        setSupportActionBar(myChildToolbar);
        myChildToolbar.setTitle("Search Results");
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState != null) {
            firstItemId = savedInstanceState.getInt(KEY_FIRST_ITEM_ID);
        }

        mMoviesDataSource = MoviesDataSource.getsInstance(this);

        mRecyclerView = findViewById(R.id.search_activity_recycler_view);
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, mRecyclerView, new MainActivity.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                //Values are passing to activity & to fragment as well
                Movie movie = mMovieList.get(position);
                Intent intent = new Intent(SearchResultsActivity.this, MovieActivity.class);
                intent.putExtra(KEY_BUNDLE, movie.getId());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        Intent i = getIntent();
        if (i != null) {
            sqlParams = (HashMap<String, String>) i.getSerializableExtra(SearchFragment.SEARCH);
            mMovieList = mMoviesDataSource.searchMovies(sqlParams);
        } else {
            mMovieList = mMoviesDataSource.limitMovies(50);
        }

        if (mMovieList.size() == 0) {
            Toast.makeText(SearchResultsActivity.this, "There are no movies matching your search criteria.", Toast.LENGTH_LONG).show();
            return;

        }

        instantiateAdapter();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshAdapter();
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    @Override
    public void onPause() {
        super.onPause();
        scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                .findFirstCompletelyVisibleItemPosition();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.grid_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.grid_view:
                if (!isGrid) {
                    isGrid = true;
                    item.setIcon(R.drawable.ic_list_white_24dp);
                } else {
                    isGrid = false;
                    item.setIcon(R.drawable.ic_grid_on_white_18dp);
                }
                refreshAdapter();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_LAYOUT_MANAGER, isGrid);
    }

    private void instantiateAdapter() {
        RecyclerView.Adapter adapter = null;
        if (isGrid) {
            adapter = new MovieAdapterGridLayout(this, mMovieList);
            mLayoutManager = new GridLayoutManager(this, getResources().getInteger(R.integer.span_items_in_grid));
        } else {
            adapter = new MovieAdapterLinearLayout(this, mMovieList);
            mLayoutManager = new LinearLayoutManager(this);
        }
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);
    }


    public void refreshAdapter() {
        instantiateAdapter();
        if (lastItemScrollPosition != 0) {
            mRecyclerView.scrollToPosition(lastItemScrollPosition);
        }
    }
}