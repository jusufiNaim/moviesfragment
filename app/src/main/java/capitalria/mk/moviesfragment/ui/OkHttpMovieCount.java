package capitalria.mk.moviesfragment.ui;


import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;

import capitalria.mk.moviesfragment.model.Example;
import capitalria.mk.moviesfragment.network.ApiService;
import capitalria.mk.moviesfragment.utils.Constants;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * OkHttpClient class that will take care of the first Api Call, this api call is necessary
 * because we need to know how many movies are in the remote database, with this number, we will
 * calculate how many times the API will be called
 * /*
 * This class is used to make a {@link OkHttpClient} call to the {@link Constants#OKHTTP_URL_LIMIT_50},
 * in order to get the number of available movies.
 * This number will be passed to the {@link ApiService}
 * Based on this number we know how many times (how many pages are available)
 * we need the Api to be called
 */

public class OkHttpMovieCount {
    private static OkHttpMovieCount sInstance;
    private OkHttpClient mOkHttpClient;
    private Request mRequest;
    private Gson mGson;

    /**
     * Callback interface that will be used to get the results back from an OkHttp call
     */
    public interface OkHttpCallback {
        void onSuccess(int result);

        void onFailure();
    }

    private OkHttpMovieCount() {
        mOkHttpClient = new OkHttpClient();
        mGson = new Gson();
        mRequest = new Request.Builder()
                .url(Constants.OKHTTP_URL_LIMIT_50)
                .build();
    }

    public static synchronized OkHttpMovieCount getsInstance() {
        if (sInstance == null)
            sInstance = new OkHttpMovieCount();
        return sInstance;
    }

    /**
     * the actual Api call.  This method will get the movie count.
     * if the response is sucessful the the int field containing the movie count will be passed
     * back to the activity that is implementing this interface;
     *
     * @param callback {@link OkHttpCallback} Interface that will works as a callback listener
     */
    public synchronized void call(OkHttpCallback callback) {
        mOkHttpClient.newCall(mRequest).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                callback.onFailure();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull final Response response) {
                if (response.isSuccessful() && response.body() != null) {
                    try {
                        Example example = mGson.fromJson(response.body().string(), Example.class);
                        callback.onSuccess(example.getData().getMovieCount());
                    } catch (JsonSyntaxException e) {
                        callback.onFailure();
                    } catch (IOException e) {
                        callback.onFailure();
                    } finally {
                        response.close();
                    }
                } else {
                    callback.onFailure();
                }
            }
        });
    }
}
