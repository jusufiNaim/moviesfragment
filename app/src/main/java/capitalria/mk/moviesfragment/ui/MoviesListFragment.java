package capitalria.mk.moviesfragment.ui;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import capitalria.mk.moviesfragment.R;
import capitalria.mk.moviesfragment.adapter.MovieAdapterGridLayout;
import capitalria.mk.moviesfragment.adapter.MovieAdapterLinearLayout;
import capitalria.mk.moviesfragment.adapter.RecyclerTouchListener;
import capitalria.mk.moviesfragment.helper.MovieSQLiteHelper;
import capitalria.mk.moviesfragment.helper.MoviesDataSource;
import capitalria.mk.moviesfragment.helper.SqlStatements;
import capitalria.mk.moviesfragment.model.Movie;


public class MoviesListFragment extends Fragment {

    public static final String KEY_BUNDLE = "bundle";
    private static final String KEY_FIRST_ITEM_ID = "firstItemId";
    private static final String KEY_SORTED = "filter";
    private static final String KEY_ITEM_ID = "itemId";
    private static final String KEY_LAYOUT_MANAGER = "grid";
    static int limit = 50;
    int scrollPosition = 0;
    int lastItemScrollPosition;
    private LinearLayoutManager mLayoutManager;
    private MoviesDataSource mMoviesDataSource;
    private String mFilter = MovieSQLiteHelper.MOVIE_INFO_KEY_ID + " DESC";
    private List<Movie> mMovieList;
    private RecyclerView mRecyclerView;
    private boolean flag_loading;
    private boolean isGrid = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMoviesDataSource = MoviesDataSource.getsInstance(getContext());
        mMovieList = mMoviesDataSource.limitMovies(limit);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.fragment_movies_list, container, false);
        mRecyclerView = rootView.findViewById(R.id.my_recycler_view);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mLayoutManager.getItemCount() - mLayoutManager.getChildCount() == mLayoutManager.findLastVisibleItemPosition() || mLayoutManager.getItemCount() - 1 == mLayoutManager.findLastVisibleItemPosition()) {
                    if (!flag_loading) {
                        flag_loading = true;
                        loadMoreData();
                    }
                }
            }
        });


        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this.getActivity(), mRecyclerView, new MainActivity.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                //Values are passing to activity & to fragment as well
                Movie movie = mMovieList.get(position);
                Intent intent = new Intent(getActivity(), MovieActivity.class);
                intent.putExtra(KEY_BUNDLE, movie.getId());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        instantiateAdapter();
        return rootView;
    }

    private void instantiateAdapter() {
        RecyclerView.Adapter adapter = null;
        if (isGrid) {
            adapter = new MovieAdapterGridLayout(getContext(), mMovieList);
            mLayoutManager = new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.span_items_in_grid));
        } else {
            adapter = new MovieAdapterLinearLayout(getContext(), mMovieList);
            mLayoutManager = new LinearLayoutManager(getActivity());
        }
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            isGrid = savedInstanceState.getBoolean(KEY_LAYOUT_MANAGER);
            mFilter = savedInstanceState.getString(KEY_SORTED);
            scrollPosition = savedInstanceState.getInt(KEY_FIRST_ITEM_ID);
            lastItemScrollPosition = savedInstanceState.getInt(KEY_ITEM_ID);
            if (limit > 50) {
                mMovieList = mMoviesDataSource.sortAndLimit(mFilter, limit);
            } else {
                mMovieList = mMoviesDataSource.limitMovies(limit);
                mRecyclerView.scrollToPosition(scrollPosition);
            }
        } else {
            scrollPosition = mLayoutManager.findFirstVisibleItemPosition();
            mMovieList = mMoviesDataSource.sortAndLimit(mFilter, limit);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString(KEY_SORTED, mFilter);
        savedInstanceState.putInt(KEY_FIRST_ITEM_ID, scrollPosition);
        savedInstanceState.putInt(KEY_ITEM_ID, lastItemScrollPosition);
        savedInstanceState.putBoolean(KEY_LAYOUT_MANAGER, isGrid);
    }


    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        refreshAdapter();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sortByName:
                mFilter = SqlStatements.sortByName(mFilter);
                mMovieList = mMoviesDataSource.sortAndLimit(mFilter, limit);
                lastItemScrollPosition = 0;
                refreshAdapter();
                return true;
            case R.id.sortByRating:
                mFilter = SqlStatements.sortByRating(mFilter);
                mMovieList = mMoviesDataSource.sortAndLimit(mFilter, limit);
                lastItemScrollPosition = 0;
                refreshAdapter();
                return true;
            case R.id.sortByYear:
                mFilter = SqlStatements.sortByYear(mFilter);
                mMovieList = mMoviesDataSource.sortAndLimit(mFilter, limit);
                lastItemScrollPosition = 0;
                refreshAdapter();
                return true;
            case R.id.sortByRecent:
                mFilter = SqlStatements.sortByMostRecent(mFilter);
                mMovieList = mMoviesDataSource.sortAndLimit(mFilter, limit);
                lastItemScrollPosition = 0;
                refreshAdapter();
                return true;
            case R.id.grid_view:
                if (!isGrid) {
                    isGrid = true;
                    item.setIcon(R.drawable.ic_list_white_24dp);
                } else {
                    isGrid = false;
                    item.setIcon(R.drawable.ic_grid_on_white_18dp);
                }
                refreshAdapter();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                .findFirstCompletelyVisibleItemPosition();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshAdapter();
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    public void refreshAdapter() {
        instantiateAdapter();
        if (lastItemScrollPosition != 0) {
            mRecyclerView.scrollToPosition(lastItemScrollPosition);
        }
    }

    public void loadMoreData() {
        limit = limit + 50;
        mMovieList = mMoviesDataSource.sortAndLimit(mFilter, limit);
        lastItemScrollPosition = mLayoutManager.findLastVisibleItemPosition() - 1;
        refreshAdapter();
        mRecyclerView.scrollToPosition(lastItemScrollPosition);
        flag_loading = false;
    }
}

