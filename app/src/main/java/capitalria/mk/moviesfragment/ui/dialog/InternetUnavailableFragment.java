package capitalria.mk.moviesfragment.ui.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import capitalria.mk.moviesfragment.R;
import capitalria.mk.moviesfragment.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class InternetUnavailableFragment extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_internet_missing)
                .setMessage(R.string.internet_missing_message)
                .setPositiveButton(R.string.enable_wifi, (dialog, id) -> startActivityForResult(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK),
                        Constants.KEY_RESULT_INTERNET))
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                    getActivity().finish();
                    System.exit(0);
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }


}
