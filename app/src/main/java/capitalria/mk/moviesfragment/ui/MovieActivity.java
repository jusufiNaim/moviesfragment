package capitalria.mk.moviesfragment.ui;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import capitalria.mk.moviesfragment.BuildConfig;
import capitalria.mk.moviesfragment.R;
import capitalria.mk.moviesfragment.helper.MoviesDataSource;
import capitalria.mk.moviesfragment.model.Movie;
import capitalria.mk.moviesfragment.model.Torrent;

public class MovieActivity extends AppCompatActivity implements YouTubePlayer.OnInitializedListener {
    public static final String PACKAGE_NAME_U_TORRENT = "https://play.google.com/store/apps/details?id=com.utorrent.client&hl=en";
    final HashMap<String, Torrent> mMap = new HashMap<>();
    private RadioButton radioButton720p, radioButton1080p, radioButton3d;
    private String youtubeTrailerCode;

    // TODO: Fix to get the torrent in proper way not in list, and construct,
    // Movie object based on selected quality then download the corresponding torrent.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);


        TextView nameTv, yearTv, ratingTv, genreTv, descriptionTv, runtimeTv;
        ImageView movieImage;
        Movie movie;
        Button downloadBtn, magnetBtn;
        Toolbar toolbar;
        MoviesDataSource moviesDataSource = MoviesDataSource.getsInstance(this);


//      The getMovie(String id) method returns List of Movies
//      including separate object for each movie.
        int b = getIntent().getIntExtra(MoviesListFragment.KEY_BUNDLE, 1);
        List<Movie> movies = moviesDataSource.getMovie(b + "");
        movie = movies.get(0);
        youtubeTrailerCode = movie.getYtTrailerCode();


        nameTv = findViewById(R.id.movie_name_TV);
        yearTv = findViewById(R.id.movie_year_TV);
        ratingTv = findViewById(R.id.movie_rating_TV);
        genreTv = findViewById(R.id.movie_genre_TV);
        descriptionTv = findViewById(R.id.movie_description_TV);
        runtimeTv = findViewById(R.id.movie_runtime_TV);
        movieImage = findViewById(R.id.movie_image);
        downloadBtn = findViewById(R.id.downloadBtn);
        magnetBtn = findViewById(R.id.magnetBtn);
        toolbar = findViewById(R.id.movie_activity_toolbar);

        radioButton720p = findViewById(R.id.quality720pRadioButton);
        radioButton1080p = findViewById(R.id.quality1080pRadioButton);
        radioButton3d = findViewById(R.id.quality3dRadioButton);


        for (Movie m : movies) {
            Torrent t = m.getTorrent();
            mMap.put(t.getQuality(), t);
        }

        if (mMap.containsKey("720p")) {
            radioButton720p.setVisibility(View.VISIBLE);
            radioButton720p.setChecked(true);
        }
        if (mMap.containsKey("1080p")) {
            radioButton1080p.setVisibility(View.VISIBLE);
            if (!radioButton720p.isChecked())
                radioButton1080p.setChecked(true);
        }
        if (mMap.containsKey("3D")) {
            radioButton3d.setVisibility(View.VISIBLE);
            if (!radioButton720p.isChecked() && !radioButton1080p.isChecked())
                radioButton3d.setChecked(true);
        }


        Picasso.with(this)
                .load(movie.getMediumCoverImage())
                .fit()
                .into(movieImage);

        nameTv.setText(movie.getTitle());
        yearTv.setText(getResources().getString(R.string.year) + " " + movie.getYear());
        ratingTv.setText(getResources().getString(R.string.rating) + " " + movie.getRating());
        genreTv.setText(getResources().getString(R.string.genre) + " " + movie.getGenre());
        descriptionTv.setText(movie.getDescriptionFull());
        runtimeTv.setText(getResources().getString(R.string.runtime) + " " + movie.getRuntime());

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(movie.getTitle());
        toolbar.setTitleTextColor(Color.WHITE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(2);
        }


        downloadBtn.setOnClickListener(v -> {
            String url = getUrlIfChecked(mMap);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            try {
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Intent.createChooser(intent, getString(R.string.continue_with));
                }
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        });

        magnetBtn.setOnClickListener(v -> {
            String url = null;
            String hash = getHashIfChecked(mMap);
            String quality = getCheckedQuality();
            try {
                url = constructMagnetLink(movie, hash, quality);
            } catch (UnsupportedEncodingException e) {
                Toast.makeText(getApplication(), getString(R.string.action_download_from_torrent_button), Toast.LENGTH_SHORT).show();
            }
            Intent torrentIntent = new Intent(Intent.ACTION_VIEW);
            torrentIntent.setData(Uri.parse(url));
            PackageManager packageManager = getPackageManager();
            if (torrentIntent.resolveActivity(packageManager) != null) {
                startActivity(torrentIntent);
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(MovieActivity.this).create();
                alertDialog.setTitle(getString(R.string.no_torrent_app_title));
                alertDialog.setMessage(getString(R.string.no_torrent_app_message));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.no_torrent_app_download),
                        (dialog, which) -> {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(PACKAGE_NAME_U_TORRENT));
                            try {
                                if (intent.resolveActivity(getPackageManager()) != null) {
                                    startActivity(intent);
                                } else {
                                    Intent.createChooser(intent, getString(R.string.continue_with));
                                }
                            } catch (ActivityNotFoundException e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                        });
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), (dialog, which) -> dialog.dismiss());
                alertDialog.show();
            }
        });

        YouTubePlayerSupportFragment frag =
                (YouTubePlayerSupportFragment) getSupportFragmentManager().findFragmentById(R.id.playerYouTube);
        if (frag != null) {
            frag.initialize(BuildConfig.YOUTUBE_KEY, this);
        }
    }

    private String getUrlIfChecked(HashMap<String, Torrent> map) {
        String url = null;
        if (radioButton720p.isChecked())
            url = map.get("720p").getUrl();
        if (radioButton1080p.isChecked())
            url = map.get("1080p").getUrl();
        if (radioButton3d.isChecked())
            url = map.get("3D").getUrl();

        return url;
    }

    private String getCheckedQuality() {
        String quality = null;
        if (radioButton720p.isChecked())
            quality = "720p";
        if (radioButton1080p.isChecked())
            quality = "1080p";
        if (radioButton3d.isChecked())
            quality = "3D";
        return quality;
    }

    private String getHashIfChecked(HashMap<String, Torrent> map) {
        String hash = null;
        if (radioButton720p.isChecked())
            hash = map.get("720p").getHash();
        if (radioButton1080p.isChecked())
            hash = map.get("1080p").getHash();
        if (radioButton3d.isChecked())
            hash = map.get("3D").getHash();

        return hash;
    }

    private String constructMagnetLink(Movie m, String hash, String quality) throws UnsupportedEncodingException {

        String baseUrl = "magnet:?xt=urn:btih:" + hash;
        String titleLong =
                m.getTitleLong()
                        + " [" + quality
                        + "]" + " [YTS.AG] ";


        StringBuilder trackersQuery = new StringBuilder(baseUrl);
        trackersQuery.append("&dn=");
        String titleQuery = URLEncoder.encode(titleLong, "utf-8");
        trackersQuery.append(titleQuery);

        String[] trackers =
                {
                        "udp://glotorrents.pw:6969/announce",
                        "udp://tracker.openbittorrent.com:80",
                        "udp://tracker.coppersurfer.tk:6969",
                        "udp://tracker.leechers-paradise.org:6969",
                        "udp://p4p.arenabg.com:1337",
                        "udp://tracker.opentrackr.org:1337/announce",
                        "udp://torrent.gresille.org:80/announce"
                };

        for (String s : trackers) {
            trackersQuery.append("&tr=");
            trackersQuery.append(URLEncoder.encode(s, "utf-8"));
        }

        return trackersQuery.toString();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.loadVideo(youtubeTrailerCode);
        youTubePlayer.addFullscreenControlFlag(BIND_ADJUST_WITH_ACTIVITY);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}


