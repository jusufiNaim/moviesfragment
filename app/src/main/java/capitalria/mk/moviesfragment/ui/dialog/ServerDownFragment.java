package capitalria.mk.moviesfragment.ui.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import capitalria.mk.moviesfragment.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServerDownFragment extends DialogFragment {


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_internet_missing)
                .setMessage(R.string.server_down)
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                    Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                    homeIntent.addCategory(Intent.CATEGORY_HOME);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(homeIntent);
                    AppCompatActivity activity = (AppCompatActivity) getActivity();
                    activity.finish();
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }


}
