package capitalria.mk.moviesfragment.ui;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import capitalria.mk.moviesfragment.dialogs.DialogManagerImpl;
import capitalria.mk.moviesfragment.dialogs.DialogManger;

public abstract class BaseActivity extends AppCompatActivity {
    protected DialogManger mDialogManger;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDialogManger = new DialogManagerImpl();
    }
}
