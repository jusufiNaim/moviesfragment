package capitalria.mk.moviesfragment.utils;

/**
 * Created by jusuf on 14.3.2018.
 */

public class Constants {

    public static final String OKHTTP_URL_LIMIT_50 = "https://yts.ag/api/v2/list_movies.json?limit=50";
    public static final String RETROFIT_BASE_URL = "https://yts.am/api/v2/";
    public static final String URL_ENDPOINT = "list_movies.json";
    public static final String URL_LIMIT_QUERY = "limit";
    public static final String URL_PAGE_QUERY = "page";
    public static final String JSON_DATA_OBJECT = "data";
    public static final String JSON_VALUE_MOVIECOUNT = "movie_count";
    public static final int KEY_RESULT_INTERNET = 5;
    public static final String TAG = "Movie Browser";

}
