package capitalria.mk.moviesfragment.helper;


import android.database.DatabaseUtils;

/**
 * Created by jusuf on 19.8.2017.
 */

public class SqlStatements {

    public static String sortByName(String filter) {
        if (filter.equals(MovieSQLiteHelper.MOVIE_INFO_KEY_TITLE + " ASC")) {
            filter = MovieSQLiteHelper.MOVIE_INFO_KEY_TITLE + " DESC";
        } else {
            filter = MovieSQLiteHelper.MOVIE_INFO_KEY_TITLE + " ASC";
        }
        return filter;
    }

    public static String sortByRating(String filter) {
        if (filter.equals(MovieSQLiteHelper.MOVIE_INFO_KEY_RATING + " DESC")) {
            filter = MovieSQLiteHelper.MOVIE_INFO_KEY_RATING + " ASC";
        } else {
            filter = MovieSQLiteHelper.MOVIE_INFO_KEY_RATING + " DESC";
        }
        return filter;
    }

    public static String sortByYear(String filter) {
        if (filter.equals(MovieSQLiteHelper.MOVIE_INFO_KEY_YEAR + " DESC")) {
            filter = MovieSQLiteHelper.MOVIE_INFO_KEY_YEAR + " ASC";
        } else {
            filter = MovieSQLiteHelper.MOVIE_INFO_KEY_YEAR + " DESC";
        }
        return filter;
    }

    public static String sortByMostRecent(String filter) {
        if (filter.equals(MovieSQLiteHelper.MOVIE_INFO_KEY_ID + " DESC")) {
            filter = MovieSQLiteHelper.MOVIE_INFO_KEY_ID + " ASC";
        } else {
            filter = MovieSQLiteHelper.MOVIE_INFO_KEY_ID + " DESC";
        }
        return filter;
    }

    public String generateOrderSql(String s) {
        String sql;
        s = TranslationHelper.translateSortByStatement(s);
        switch (s) {
            case "alphabetical":
                sql = MovieSQLiteHelper.MOVIE_INFO_KEY_TITLE + " ASC";
                break;
            case "year":
                sql = MovieSQLiteHelper.MOVIE_INFO_KEY_YEAR + " DESC";
                break;
            case "rating":
                sql = MovieSQLiteHelper.MOVIE_INFO_KEY_RATING + " DESC";
                break;
            case "latest":
                sql = MovieSQLiteHelper.MOVIE_INFO_KEY_ID + " DESC";
                break;
            default:
                sql = MovieSQLiteHelper.MOVIE_INFO_KEY_ID + " DESC";
        }
        return sql;
    }

    public String generateRatingSql(String s) {
        String sql;
        s = TranslationHelper.translateAllStatement(s);
        if (s.equalsIgnoreCase("All")) {
            sql = " IS NOT NULL";
        } else {
            sql = " >= " + s;
        }
        return sql;
    }

    public String generateGenreSql(String s) {
        String sql;
        s = TranslationHelper.translateAllStatement(s);
        if (s.equalsIgnoreCase("All")) {
            sql = " IS NOT NULL";
        } else {
            s = TranslationHelper.translateGenreStatement(s);
            sql = " LIKE " + " '%" + s + "%' ";
        }
        return sql;

    }

    public String generateQualitySql(String s) {
        String sql = null;
        s = TranslationHelper.translateAllStatement(s);
        if (s.equalsIgnoreCase("All")) {
            sql = " IS NOT NULL";
        } else if (s.equalsIgnoreCase("720p")) {
            sql = " LIKE '720p'";
        } else if (s.equalsIgnoreCase("1080p")) {
            sql = " LIKE '1080p'";
        } else if (s.equalsIgnoreCase("3d")) {
            sql = " LIKE '3d'";
        }
        return sql;
    }

    public String generateNameSql(String s) {
        String sql;
        if (!s.isEmpty()) {
            s = s.trim();
            sql = "%" + s + "%";
            sql = " LIKE " + DatabaseUtils.sqlEscapeString(sql);
        } else {
            sql = " IS NOT NULL";
        }
        return sql;
    }
}
