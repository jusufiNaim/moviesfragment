package capitalria.mk.moviesfragment.helper;

/**
 * Created by jusuf on 09.1.2018.
 */

public class TranslationHelper {

    public static String translateAllStatement(String param) {
        if (param != null)
            if (param.equalsIgnoreCase("Të gjitha") ||
                    param.equalsIgnoreCase("Alle") || param.equalsIgnoreCase("сите")) {
                param = "All";
                return param;
            } else {
                return param;
            }
        return null;
    }

    public static String translateSortByStatement(String param) {
        if (param != null) {
            if (param.equalsIgnoreCase("alfabetit")
                    || param.equalsIgnoreCase("alphabetisch")
                    || param.equalsIgnoreCase("азбучен ред")) {
                param = "alphabetical";
            } else if (param.equalsIgnoreCase("vitit")
                    || param.equalsIgnoreCase("jahr")
                    || param.equalsIgnoreCase("година")) {
                param = "year";
            } else if (param.equalsIgnoreCase("më të reja")
                    || param.equalsIgnoreCase("neuesten")
                    || param.equalsIgnoreCase("најнови")) {
                param = "latest";
            } else if (param.equalsIgnoreCase("vlerësimit")
                    || param.equalsIgnoreCase("bewertung")
                    || param.equalsIgnoreCase("оценка")) {
                param = "rating";
            }
            return param;
        } else return null;
    }

    public static String translateGenreStatement(String param) {
        if (param != null) {
            if (param.equalsIgnoreCase("Akcion")
                    || param.equalsIgnoreCase("Action")
                    || param.equalsIgnoreCase("Акција")) {
                param = "Action";
            } else if (param.equalsIgnoreCase("Aventurë")
                    || param.equalsIgnoreCase("Abenteuer")
                    || param.equalsIgnoreCase("Авантура")) {
                param = "Adventure";
            } else if (param.equalsIgnoreCase("Animacion")
                    || param.equalsIgnoreCase("Animation")
                    || param.equalsIgnoreCase("Анимиран")) {
                param = "Animation";
            } else if (param.equalsIgnoreCase("Biografik")
                    || param.equalsIgnoreCase("Biografie")
                    || param.equalsIgnoreCase("Биографија")) {
                param = "Biography";
            } else if (param.equalsIgnoreCase("Komedi")
                    || param.equalsIgnoreCase("Komödie")
                    || param.equalsIgnoreCase("Комедија")) {
                param = "Comedy";
            } else if (param.equalsIgnoreCase("Krim")
                    || param.equalsIgnoreCase("Kriminalistisch")
                    || param.equalsIgnoreCase("Криминалистички")) {
                param = "Crime";
            } else if (param.equalsIgnoreCase("Dokumentar")
                    || param.equalsIgnoreCase("Dokumentarfilm")
                    || param.equalsIgnoreCase("Документарен")) {
                param = "Documentary";
            } else if (param.equalsIgnoreCase("Dramë")
                    || param.equalsIgnoreCase("Drama")
                    || param.equalsIgnoreCase("Драма")) {
                param = "Drama";
            } else if (param.equalsIgnoreCase("Familiar")
                    || param.equalsIgnoreCase("Familie")
                    || param.equalsIgnoreCase("Семеен")) {
                param = "Family";
            } else if (param.equalsIgnoreCase("Fantastik")
                    || param.equalsIgnoreCase("Fantasie")
                    || param.equalsIgnoreCase("Фантазија")) {
                param = "Fantasy";
            } else if (param.equalsIgnoreCase("Film-Noir")
                    || param.equalsIgnoreCase("Film-Noir")
                    || param.equalsIgnoreCase("Film-Noir")) {
                param = "Film-Noir";
            } else if (param.equalsIgnoreCase("Lojë-Shou")
                    || param.equalsIgnoreCase("Spielshow")
                    || param.equalsIgnoreCase("Игран-шоу")) {
                param = "Game-Show";
            } else if (param.equalsIgnoreCase("Historik")
                    || param.equalsIgnoreCase("Geschichte")
                    || param.equalsIgnoreCase("Историски")) {
                param = "History";
            } else if (param.equalsIgnoreCase("Horor")
                    || param.equalsIgnoreCase("Horrorfilm")
                    || param.equalsIgnoreCase("Хорор")) {
                param = "Horror";
            } else if (param.equalsIgnoreCase("Muzikë")
                    || param.equalsIgnoreCase("Music")
                    || param.equalsIgnoreCase("Музички")) {
                param = "Music";
            } else if (param.equalsIgnoreCase("Muzikor")
                    || param.equalsIgnoreCase("Musical")
                    || param.equalsIgnoreCase("Музикл")) {
                param = "Musical";
            } else if (param.equalsIgnoreCase("Mister")
                    || param.equalsIgnoreCase("Mysteriös")
                    || param.equalsIgnoreCase("Мистерија")) {
                param = "Mystery";
            } else if (param.equalsIgnoreCase("Lajme")
                    || param.equalsIgnoreCase("Nachrichten")
                    || param.equalsIgnoreCase("Новости")) {
                param = "News";
            } else if (param.equalsIgnoreCase("TV-real")
                    || param.equalsIgnoreCase("Reality-TV")
                    || param.equalsIgnoreCase("Реална-TV")) {
                param = "Reality-TV";
            } else if (param.equalsIgnoreCase("Romansë")
                    || param.equalsIgnoreCase("Romantik")
                    || param.equalsIgnoreCase("Романтика")) {
                param = "Romance";
            } else if (param.equalsIgnoreCase("Fantazi shkencore")
                    || param.equalsIgnoreCase("Sci-Fi")
                    || param.equalsIgnoreCase("Научна фантастика")) {
                param = "Sci-Fi";
            } else if (param.equalsIgnoreCase("Sport")
                    || param.equalsIgnoreCase("Sport")
                    || param.equalsIgnoreCase("Спорт")) {
                param = "Sport";
            } else if (param.equalsIgnoreCase("Debat")
                    || param.equalsIgnoreCase("Talkshow")
                    || param.equalsIgnoreCase("Толкшоу")) {
                param = "Talk-show";
            } else if (param.equalsIgnoreCase("Thriller")
                    || param.equalsIgnoreCase("Thriller")
                    || param.equalsIgnoreCase("Трилер")) {
                param = "Thriller";
            } else if (param.equalsIgnoreCase("Luftarak")
                    || param.equalsIgnoreCase("Krieg")
                    || param.equalsIgnoreCase("Воен")) {
                param = "War";
            } else if (param.equalsIgnoreCase("Western")
                    || param.equalsIgnoreCase("Western")
                    || param.equalsIgnoreCase("Вестерн")) {
                param = "Western";
            } else {
                param = "Action";
            }
            return param;
        } else return null;
    }
}
